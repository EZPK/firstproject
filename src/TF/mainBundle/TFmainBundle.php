<?php

namespace TF\mainBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TFmainBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
