<?php

namespace TF\mainBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TF\mainBundle\Entity\Hotel;
use TF\mainBundle\Form\HotelType;

class HotelController extends Controller
{

    /**
     * @Security("has_role('ROLE_ADMIN')")
     */

    public function addAction(Request $request)
    {

        $hotel = new Hotel();

        $form = $this->createForm(HotelType::class, $hotel);
        $form->handleRequest($request);


        if($form->isValid()){

            $hotel->getMainPicture()->upload();
            $hotel->getMainPicture()->setAlt("mainPicture");
            foreach($hotel->getPictures() as $p)
            {
                $p->setAlt('picture');
                $p->upload();
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($hotel);
            $em->flush();


           $this->addFlash('success_messages', 'votre hotel est bien enregistré');
           return $this->redirectToRoute("tf_main_home");
        }

        return $this->render('TFmainBundle:Hotel:add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function getHotelAction()
    {
        $em = $this->getDoctrine()->getManager()->getRepository("TFmainBundle:Hotel");
        $listOfHotel = $em->findWithLimit(5);
        //$listOfHotel = array_slice($listOfHotel, -5);

        return $this->render('TFmainBundle:Default:home.html.twig', array(
            'hotels' => $listOfHotel
        ));
    }

    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager()->getRepository("TFmainBundle:Hotel");
        $listOfHotel = $em->findOneHotelWithPictures($id);
        dump($listOfHotel);

        return $this->render('TFmainBundle:Hotel:hotel.html.twig', array(
            'hotels' => $listOfHotel
        ));
    }

//    public function deleteHotelAction($id)
//    {
//        $em = $this->getDoctrine()->getManager()->getRepository("TFmainBundle:Hotel");
//        $deleteHotel = $em->find($id);
//
//    }

}
