<?php

namespace TF\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use TF\mainBundle\Entity\User;
use TF\mainBundle\Form\UserType;
use TF\mainBundle\TFmainBundle;

class RegistrationController extends Controller
{
    public function registerAction(Request $request)
    {

        $user = new User();
        $form = $this->createForm(UserType::Class, $user);
        $form->handleRequest($request);

        if($form->isValid()){
            $user->getAvatar()->upload();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute("tf_main_home");
        }

        return $this->render("TFmainBundle:User:register.html.twig",
            array('form'=>$form->createView())
        );
    }



}
