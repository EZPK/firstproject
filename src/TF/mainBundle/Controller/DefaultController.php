<?php

namespace TF\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function homeAction()
    {
        return $this->render(
            'TFmainBundle:Default:home.html.twig'
        );
    }

    public function contactAction()
    {
        return $this->render(
            'TFmainBundle:Default:contact.html.twig'
        );
    }

    public function aboutAction()
    {
        return $this->render(
            'TFmainBundle:Default:about.html.twig'
        );
    }
}
