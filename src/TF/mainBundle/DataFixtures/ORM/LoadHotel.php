<?php
// src/AppBundle/DataFixtures/ORM/LoadUserData.php

namespace TFmainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TF\MainBundle\Entity\Hotel;



class LoadHotelData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $Hotels = array(
//            array("Maria Del Sol", "johannesburg", "135"),
//            array("Trump Tower", "New York", "9000"),
//            array("Camping du lac", "Cerfontaine", "2"),
        );

        foreach ($Hotels as $i=>$Hotel){
            $newHotel = new Hotel();
            $newHotel->setName($Hotel[0]);
            $newHotel->setCity($Hotel[1]);
            $newHotel->setPrice($Hotel[2]);

            $manager->persist($newHotel);
        }


        $manager->flush();

    }
}
?>