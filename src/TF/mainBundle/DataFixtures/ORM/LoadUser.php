<?php
// src/AppBundle/DataFixtures/ORM/LoadUserData.php

namespace TFmainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use TF\mainBundle\Entity\Pictures;
use TF\MainBundle\Entity\User;



class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $admin = new User();

        $encoder = new messageDigestPasswordEncoder('sha512', true, 4756);


        $password1 = $encoder->encodePassword("test", $user->getSalt());
        $password2 = $encoder->encodePassword("bonjour", $admin->getSalt());

        $user->setLastName("user");
        $user->setFirstName("user");
        $user->setPassword($password1);
        $user->setRoles(array("ROLE_USER"));
        $user->setEmail("user@user.be");

        $admin->setLastName("admin");
        $admin->setFirstName("admin");
        $admin->setPassword($password2);
        $admin->setRoles(array("ROLE_ADMIN"));
        $admin->setEmail("admin@admin.be");

        $manager->persist($user);
        $manager->persist($admin);


        $manager->flush();

    }
}
?>