<?php

namespace TF\mainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 * @UniqueEntity("username",message="Le username existe déjà")
 * @UniqueEntity("email",message="Le mail existe déjà")
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="TF\mainBundle\Repository\UserRepository")
 *
 */
class User extends BaseUser
{

    /**
     * @var
     */
    protected $username;

    /**
     * @var Booking
     * @ORM\OneToOne(targetEntity="TF\mainBundle\Entity\Booking", cascade={"persist","remove"})
     */
    private $booking;

    /**
     * @var Pictures
     * @ORM\OneToOne(targetEntity="TF\mainBundle\Entity\Pictures", cascade={"persist","remove"})
     */
    private $Avatar;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        $this->salt =  uniqid();
        $this->addRole('ROLE_USER');
        $this->enabled = true;

    }


    /**
     * @return Pictures
     */
    public function getAvatar()
    {
        return $this->Avatar;
    }

    /**
     * @param Pictures $Avatar
     */
    public function setAvatar(Pictures $Avatar)
    {
        $this->Avatar = $Avatar;
        $this->Avatar->setAlt($this->username);
    }

    /**
     * @return Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * @param Booking $booking
     */
    public function setBooking($booking)
    {
        $this->booking = $booking;
    }

}

