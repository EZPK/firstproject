<?php

namespace TF\mainBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Hotel
 *
 * @ORM\Table(name="hotel")
 * @ORM\Entity(repositoryClass="TF\mainBundle\Repository\HotelRepository")
 */
class Hotel
{


    /**
     * @var Options
     * @ORM\OneToOne(targetEntity="TF\mainBundle\Entity\Options", cascade={"persist","remove"})
     */
    private $Options;

    /**
     * @var Country
     * @ORM\ManyToOne(targetEntity="TF\mainBundle\Entity\Country")
     */
    private $Country;

    /**
     * @var Pictures
     * @ORM\OneToOne(targetEntity="TF\mainBundle\Entity\Pictures", cascade={"persist","remove"})
     */
    private $MainPicture;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="TF\mainBundle\Entity\Pictures", mappedBy="hotel", cascade={"persist","remove"})
     */
    private $Pictures;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Tu t'es bien fait niquer HAHAHA")
     * @ORM\Column(name="Name", type="string", length=255)
     * @Assert\Length(max=25, maxMessage="yopo", min=2)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="City", type="string", length=255)
     */
    private $city;

    /**
     * @var float
     *
     * @ORM\Column(name="Price", type="float")
     */
    private $price;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Hotel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Hotel
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Hotel
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return pictures
     */
    public function getMainPicture()
    {
        return $this->MainPicture;
    }

    /**
     * @param pictures $MainPicture
     */
    public function setMainPicture($MainPicture)
    {
        $this->MainPicture = $MainPicture;
    }

    /**
     *@param pictures $Pictures
     */
    public function addPicture(Pictures $Pictures)
    {
        $this->Pictures[] = $Pictures;
        $Pictures->setHotel($this);
    }

    /**
     *@param pictures $Pictures
     */

    public function removePicture(Pictures $Pictures)
    {
        $key = array_search($this->Pictures, $Pictures);
        if($key !== null){
            unset($this->Pictures[$key]);
        }
    }

    public function __construct()
    {

        $this->Pictures = array();
    }

    /**
     * @return array
     */
    public function getPictures()
    {
        return $this->Pictures;
    }

    /**
     * @param array $Pictures
     */
    public function setPictures($Pictures)
    {
        $this->Pictures = $Pictures;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * @param mixed $Country
     */
    public function setCountry($Country)
    {
        $this->Country = $Country;
    }

    /**
     * @return Options
     */
    public function getOptions()
    {
        return $this->Options;
    }

    /**
     * @param Options $Options
     */
    public function setOptions($Options)
    {
        $this->Options = $Options;
    }
}

