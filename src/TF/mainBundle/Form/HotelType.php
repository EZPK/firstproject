<?php

namespace TF\mainBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HotelType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::Class, array(
                'required' => true,
            ))
                ->add('city', TextType::Class, array(
                    'required' => true,
                ))
                ->add('price', NumberType::Class, array(
                    'required' => true,
                ))
                ->add('Options', OptionsType::Class)
                ->add('Country', EntityType::Class, array(
                    'choice_label' => 'name',
                    'class' => 'TF\mainBundle\Entity\Country',
                    'multiple' => false,
                    'expanded' => false,
                    'label' => 'Pays',
                    'required' => false,
                    'attr' => array(
                        "class" => "maClasse"
                    ),
                'label_attr' => array()

                ))
                ->add('MainPicture', PicturesType::class)
                ->add('pictures', CollectionType::class, array(
                        'entry_type' => PicturesType::class,
                        'allow_add' => true,
                        'allow_delete' => true
                ))
                ->add('submit', SubmitType::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TF\mainBundle\Entity\Hotel'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'tf_mainbundle_hotel';
    }


}
